# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the live-boot package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: live-boot VERSION\n"
"POT-Creation-Date: 2010-06-05 14:11+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

#. type: TH
#: en/live-boot.7:1 en/live-snapshot.1:1
#, no-wrap
msgid "LIVE-BOOT"
msgstr ""

#. type: TH
#: en/live-boot.7:1 en/live-snapshot.1:1
#, no-wrap
msgid "2010-06-05"
msgstr ""

#. type: TH
#: en/live-boot.7:1 en/live-snapshot.1:1
#, no-wrap
msgid "2.0~a5"
msgstr ""

#. type: TH
#: en/live-boot.7:1 en/live-snapshot.1:1
#, no-wrap
msgid "Debian Live Project"
msgstr ""

#. type: SH
#: en/live-boot.7:3 en/live-snapshot.1:3
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: en/live-boot.7:5
msgid "B<live-boot> - System Boot Scripts"
msgstr ""

#. type: SH
#: en/live-boot.7:6 en/live-snapshot.1:17
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: en/live-boot.7:8
msgid ""
"B<live-boot> contains the scripts that configure a Debian Live system during "
"the boot process (early userspace)."
msgstr ""

#.  FIXME
#. type: Plain text
#: en/live-boot.7:11
msgid ""
"live-boot is a hook for the initramfs-tools, used to generate a initramfs "
"capable to boot live systems, such as those created by I<live-helper>(7). "
"This includes the Debian Live isos, netboot tarballs, and usb stick images."
msgstr ""

#.  FIXME
#. type: Plain text
#: en/live-boot.7:14
msgid ""
"At boot time it will look for a (read-only) media containing a \"/live\" "
"directory where a root filesystems (often a compressed filesystem image like "
"squashfs) is stored. If found, it will create a writable environment, using "
"aufs, for Debian like systems to boot from."
msgstr ""

#. type: SH
#: en/live-boot.7:15
#, no-wrap
msgid "CONFIGURATION"
msgstr ""

#. type: Plain text
#: en/live-boot.7:17
msgid ""
"B<live-boot> can be configured through a boot parameter or a configuration "
"file."
msgstr ""

#. type: SS
#: en/live-boot.7:18
#, no-wrap
msgid "Kernel Parameters"
msgstr ""

#. type: Plain text
#: en/live-boot.7:20
msgid ""
"B<live-boot> is only activated if 'boot=live' was used as a kernel parameter."
msgstr ""

#. type: Plain text
#: en/live-boot.7:22
msgid ""
"In addition, there are some more boot parameters to influence the behaviour, "
"see below."
msgstr ""

#. type: SS
#: en/live-boot.7:23
#, no-wrap
msgid "Configuration Files"
msgstr ""

#. type: Plain text
#: en/live-boot.7:25
msgid ""
"B<live-boot> can be configured (but not activated) through configuration "
"files. Those files can be placed either in the root filesystem itself (/etc/"
"live/boot.conf, /etc/live/boot.conf.d/), or on the live media (live/boot."
"conf, live/boot.conf.d/)."
msgstr ""

#. type: SH
#: en/live-boot.7:26 en/live-snapshot.1:20
#, no-wrap
msgid "OPTIONS"
msgstr ""

#.  FIXME
#. type: Plain text
#: en/live-boot.7:29
msgid "B<live-boot> currently features the following parameters."
msgstr ""

#. type: IP
#: en/live-boot.7:29
#, no-wrap
msgid "B<access>=I<ACCESS>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:31
msgid ""
"Set the accessibility level for physically or visually impared users. ACCESS "
"must be one of v1, v2, v3, m1, or m2. v1=lesser visual impairment, "
"v2=moderate visual impairment, v3=blindness, m1=minor motor difficulties, "
"m2=moderate motor difficulties."
msgstr ""

#. type: IP
#: en/live-boot.7:31
#, no-wrap
msgid "B<console>=I<TTY,SPEED>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:33
msgid ""
"Set the default console to be used with the \"live-getty\" option. Example: "
"\"console=ttyS0,115200\""
msgstr ""

#. type: IP
#: en/live-boot.7:33
#, no-wrap
msgid "B<debug>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:35
msgid "Makes initramfs boot process more verbose."
msgstr ""

#. type: Plain text
#: en/live-boot.7:37
msgid "Use: debug=1"
msgstr ""

#. type: Plain text
#: en/live-boot.7:39
msgid "Without setting debug to a value the messages may not be shown."
msgstr ""

#. type: IP
#: en/live-boot.7:39
#, no-wrap
msgid "B<fetch>=I<URL>"
msgstr ""

#. type: IP
#: en/live-boot.7:40
#, no-wrap
msgid "B<httpfs>=I<URL>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:48
msgid ""
"Another form of netboot by downloading a squashfs image from a given url.  "
"The fetch method copies the image to ram and the httpfs method uses fuse and "
"httpfs2 to mount the image in place. Copying to ram requires more memory and "
"might take a long time for large images. However, it is more likely to work "
"correctly because it does not require networking afterwards and the system "
"operates faster once booted because it does not require to contact the "
"server anymore."
msgstr ""

#. type: Plain text
#: en/live-boot.7:50
msgid ""
"Due to current limitations in busyboxs wget and DNS resolution, an URL can "
"not contain a hostname but an IP only."
msgstr ""

#. type: Plain text
#: en/live-boot.7:52
msgid "Not working: http://example.com/path/to/your_filesystem.squashfs"
msgstr ""

#. type: Plain text
#: en/live-boot.7:54
msgid "Working: http://1.2.3.4/path/to/your_filesystem.squashfs"
msgstr ""

#. type: Plain text
#: en/live-boot.7:56
msgid ""
"Also note that therefore it's currently not possible to fetch an image from "
"a namebased virtualhost of an httpd if it is sharing the ip with the main "
"httpd instance."
msgstr ""

#. type: Plain text
#: en/live-boot.7:58
msgid "You may also use the live iso image in place of the squashfs image."
msgstr ""

#. type: IP
#: en/live-boot.7:58
#, no-wrap
msgid "B<ignore_uuid>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:60
msgid ""
"Do not check that any UUID embedded in the initramfs matches the discovered "
"medium. live-boot may be told to generate a UUID by setting "
"LIVE_GENERATE_UUID=1 when building the initramfs."
msgstr ""

#. type: IP
#: en/live-boot.7:60
#, no-wrap
msgid "B<integrity-check>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:62
msgid ""
"If specified, an MD5 sum is calculated on the live media during boot and "
"compared to the value found in md5sum.txt found in the root directory of the "
"live media."
msgstr ""

#. type: IP
#: en/live-boot.7:62
#, no-wrap
msgid "B<ip>=[I<CLIENT_IP>]:[I<SERVER_IP>]:[I<GATEWAY_IP>]:[I<NETMASK>]:[I<HOSTNAME>]:[I<DEVICE>]:[I<AUTOCONF>] [,[I<CLIENT_IP>]:[I<SERVER_IP>]:[I<GATEWAY_IP>]:[I<NETMASK>]:[I<HOSTNAME>]:[I<DEVICE>]:[I<AUTOCONF>]]"
msgstr ""

#. type: Plain text
#: en/live-boot.7:64
msgid ""
"Let you specify the name(s) and the options of the interface(s) that should "
"be configured at boot time. Do not specify this if you want to use dhcp "
"(default). It will be changed in a future release to mimick official kernel "
"boot param specification (e.g. ip=10.0.0.1::10.0.0.254:255.255.255.0::"
"eth0,:::::eth1:dhcp)."
msgstr ""

#. type: IP
#: en/live-boot.7:64
#, no-wrap
msgid "B<ip>=[I<frommedia>]"
msgstr ""

#. type: Plain text
#: en/live-boot.7:66
msgid ""
"If this variable is set, dhcp and static configuration are just skipped and "
"the system will use the (must be) media-preconfigured /etc/network/"
"interfaces instead."
msgstr ""

#. type: IP
#: en/live-boot.7:66
#, no-wrap
msgid "{B<keyb>|B<kbd-chooser/method>}=I<KEYBOARD>, {B<klayout>|B<console-setup/layoutcode>}=I<LAYOUT>, {B<kvariant>|B<console-setup/variantcode>}=I<VARIANT>, {B<kmodel>I<|>B<console-setup/modelcode>}=I<CODE>, B<koptions>=I<OPTIONS>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:68
msgid ""
"Configure the running keyboard as specified, if this one misses live-boot "
"behaves as if \"keyb=us\" was specified. It will be interfered from \"locale="
"\" if locale is only 2 lowecase letters as a special case. You could also "
"specify console layout, variant, code, and options (no defaults)."
msgstr ""

#. type: IP
#: en/live-boot.7:68
#, no-wrap
msgid "B<live-getty>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:70
msgid ""
"This changes the auto-login on virtual terminals to use the (experimental) "
"live-getty code. With this option set the standard kernel argument \"console="
"\" is parsed and if a serial console is specified then live-getty is used to "
"autologin on the serial console."
msgstr ""

#. type: IP
#: en/live-boot.7:70
#, no-wrap
msgid "{B<live-media>|B<bootfrom>}=I<DEVICE>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:72
msgid ""
"If you specify one of this two equivalent forms, live-boot will first try to "
"find this device for the \"/live\" directory where the read-only root "
"filesystem should reside. If it did not find something usable, the normal "
"scan for block devices is performed."
msgstr ""

#. type: Plain text
#: en/live-boot.7:74
msgid ""
"Instead of specifing an actual device name, the keyword 'removable' can be "
"used to limit the search of acceptable live media to removable type only. "
"Note that if you want to further restrict the media to usb mass storage "
"only, you can use the 'removable-usb' keyword."
msgstr ""

#. type: IP
#: en/live-boot.7:74
#, no-wrap
msgid "{B<live-media-encryption>|B<encryption>}=I<TYPE>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:76
msgid ""
"live-boot will mount the encrypted rootfs TYPE, asking the passphrase, "
"useful to build paranoid live systems :-). TYPE supported so far are \"aes\" "
"for loop-aes encryption type."
msgstr ""

#. type: IP
#: en/live-boot.7:76
#, no-wrap
msgid "B<live-media-offset>=I<BYTES>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:78
msgid ""
"This way you could tell live-boot that your image starts at offset BYTES in "
"the above specified or autodiscovered device, this could be useful to hide "
"the Debian Live iso or image inside another iso or image, to create \"clean"
"\" images."
msgstr ""

#. type: IP
#: en/live-boot.7:78
#, no-wrap
msgid "B<live-media-path>=I<PATH>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:80
msgid ""
"Sets the path to the live filesystem on the medium. By default, it is set to "
"'/live' and you should not change that unless you have customized your media "
"accordingly."
msgstr ""

#. type: IP
#: en/live-boot.7:80
#, no-wrap
msgid "B<live-media-timeout>=I<SECONDS>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:82
msgid ""
"Set the timeout in seconds for the device specified by \"live-media=\" to "
"become ready before giving up."
msgstr ""

#. type: IP
#: en/live-boot.7:82
#, no-wrap
msgid "B<module>=I<NAME>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:84
msgid ""
"Instead of using the default optional file \"filesystem.module\" (see below) "
"another file could be specified without the extension \".module\"; it should "
"be placed on \"/live\" directory of the live medium."
msgstr ""

#. type: IP
#: en/live-boot.7:84
#, no-wrap
msgid "B<netboot>[=nfs|cifs]"
msgstr ""

#. type: Plain text
#: en/live-boot.7:86
msgid ""
"This tells live-boot to perform a network mount. The parameter \"nfsroot="
"\" (with optional \"nfsopts=\"), should specify where is the location of the "
"root filesystem.  With no args, will try cifs first, and if it fails nfs."
msgstr ""

#. type: IP
#: en/live-boot.7:86
#, no-wrap
msgid "B<nfsopts>="
msgstr ""

#. type: Plain text
#: en/live-boot.7:88
msgid "This lets you specify custom nfs options."
msgstr ""

#. type: IP
#: en/live-boot.7:88
#, no-wrap
msgid "B<nofastboot>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:90
msgid ""
"This parameter disables the default disabling of filesystem checks in /etc/"
"fstab. If you have static filesystems on your harddisk and you want them to "
"be checked at boot time, use this parameter, otherwise they are skipped."
msgstr ""

#. type: IP
#: en/live-boot.7:90
#, no-wrap
msgid "B<nopersistent>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:92
msgid ""
"disables the \"persistent\" feature, useful if the bootloader (like "
"syslinux) has been installed with persistent enabled."
msgstr ""

#. type: IP
#: en/live-boot.7:92
#, no-wrap
msgid "B<noprompt>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:94
msgid "Do not prompt to eject the CD or remove the USB flash drive on reboot."
msgstr ""

#. type: IP
#: en/live-boot.7:94
#, no-wrap
msgid "B<swapon>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:96
msgid "This parameter enables usage of local swap partitions."
msgstr ""

#. type: IP
#: en/live-boot.7:96
#, no-wrap
msgid "B<noxautoconfig>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:98
msgid ""
"This parameter disables Xorg auto-reconfiguration at boot time. This is "
"valuable if you either do the detection on your own, or, if you want to ship "
"a custom, premade xorg.conf in your live system."
msgstr ""

#. type: IP
#: en/live-boot.7:98
#, no-wrap
msgid "B<persistent>[=nofiles]"
msgstr ""

#. type: Plain text
#: en/live-boot.7:100
msgid ""
"live-boot will look for persistent and snapshot partitions or files labeled "
"\"live-rw\", \"home-rw\", and files called \"live-sn*\", \"home-sn*\" and "
"will try to, in order: mount as /cow the first, mount the second in /home, "
"and just copy the contents of the latter in appropriate locations "
"(snapshots). Snapshots will be tried to be updated on reboot/shutdown. Look "
"at live-snapshot(1) for more informations. If \"nofiles\" is specified, only "
"filesystems with matching labels will be searched; no filesystems will be "
"traversed looking for archives or image files. This results in shorter boot "
"times."
msgstr ""

#. type: IP
#: en/live-boot.7:100
#, no-wrap
msgid "B<persistent-path>=I<PATH>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:102
msgid ""
"live-boot will look for persistency files in the root directory of a "
"partition, with this parameter, the path can be configured so that you can "
"have multiple directories on the same partition to store persistency files."
msgstr ""

#. type: IP
#: en/live-boot.7:102
#, no-wrap
msgid "{B<preseed/file>|B<file>}=I<FILE>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:104
msgid ""
"A path to a file present on the rootfs could be used to preseed debconf "
"database."
msgstr ""

#. type: IP
#: en/live-boot.7:104
#, no-wrap
msgid "B<package/question>=I<VALUE>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:106
msgid ""
"All debian installed packages could be preseeded from command-line that way, "
"beware of blanks spaces, they will interfere with parsing, use a preseed "
"file in this case."
msgstr ""

#. type: IP
#: en/live-boot.7:106
#, no-wrap
msgid "B<quickreboot>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:108
msgid ""
"This option causes live-boot to reboot without attempting to eject the media "
"and without asking the user to remove the boot media."
msgstr ""

#. type: IP
#: en/live-boot.7:108
#, no-wrap
msgid "B<showmounts>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:110
msgid ""
"This parameter will make live-boot to show on \"/\" the ro filesystems "
"(mostly compressed) on \"/live\". This is not enabled by default because "
"could lead to problems by applications like \"mono\" which store binary "
"paths on installation."
msgstr ""

#. type: IP
#: en/live-boot.7:110
#, no-wrap
msgid "B<silent>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:112
msgid ""
"If you boot with the normal quiet parameter, live-boot hides most messages "
"of its own. When adding silent, it hides all."
msgstr ""

#. type: IP
#: en/live-boot.7:112
#, no-wrap
msgid "B<textonly>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:114
msgid ""
"Start up to text-mode shell prompts, disabling the graphical user interface."
msgstr ""

#. type: IP
#: en/live-boot.7:114
#, no-wrap
msgid "B<todisk>=I<DEVICE>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:116
msgid ""
"Adding this parameter, live-boot will try to copy the entire read-only media "
"to the specified device before mounting the root filesystem. It probably "
"needs a lot of free space. Subsequent boots should then skip this step and "
"just specify the \"live-media=DEVICE\" boot parameter with the same DEVICE "
"used this time."
msgstr ""

#. type: IP
#: en/live-boot.7:116
#, no-wrap
msgid "B<toram>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:118
msgid ""
"Adding this parameter, live-boot will try to copy the whole read-only media "
"to the computer's RAM before mounting the root filesystem. This could need a "
"lot of ram, according to the space used by the read-only media."
msgstr ""

#. type: IP
#: en/live-boot.7:118
#, no-wrap
msgid "B<union>=aufs|unionfs"
msgstr ""

#. type: Plain text
#: en/live-boot.7:120
msgid ""
"By default, live-boot uses aufs. With this parameter, you can switch to "
"unionfs."
msgstr ""

#. type: IP
#: en/live-boot.7:120
#, no-wrap
msgid "B<xdebconf>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:122
msgid ""
"Uses xdebconfigurator, if present on the rootfs, to configure X instead of "
"the standard procedure (experimental)."
msgstr ""

#. type: IP
#: en/live-boot.7:122
#, no-wrap
msgid "B<xvideomode>=I<RESOLUTION>"
msgstr ""

#.  FIXME
#. type: Plain text
#: en/live-boot.7:125
msgid "Doesn't do xorg autodetection, but enforces a given resolution."
msgstr ""

#.  FIXME
#. type: SH
#: en/live-boot.7:127
#, no-wrap
msgid "FILES (old)"
msgstr ""

#. type: IP
#: en/live-boot.7:128 en/live-snapshot.1:43
#, no-wrap
msgid "B</etc/live.conf>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:130 en/live-snapshot.1:45
msgid ""
"Some variables can be configured via this config file (inside the live "
"system)."
msgstr ""

#. type: IP
#: en/live-boot.7:130 en/live-snapshot.1:45
#, no-wrap
msgid "B<live/filesystem.module>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:132 en/live-snapshot.1:47
msgid ""
"This optional file (inside the live media) contains a list of white-space or "
"carriage-return-separated file names corresponding to disk images in the \"/"
"live\" directory. If this file exists, only images listed here will be "
"merged into the root aufs, and they will be loaded in the order listed here. "
"The first entry in this file will be the \"lowest\" point in the aufs, and "
"the last file in this list will be on the \"top\" of the aufs, directly "
"below /cow.  Without this file, any images in the \"/live\" directory are "
"loaded in alphanumeric order."
msgstr ""

#. type: IP
#: en/live-boot.7:132 en/live-snapshot.1:47
#, no-wrap
msgid "B</etc/live-persistence.binds>"
msgstr ""

#. type: Plain text
#: en/live-boot.7:134 en/live-snapshot.1:49
msgid ""
"This optional file (which resides in the rootfs system, not in the live "
"media) is used as a list of directories which not need be persistent: ie. "
"their content does not need to survive reboots when using the persistence "
"features."
msgstr ""

#. type: Plain text
#: en/live-boot.7:137 en/live-snapshot.1:51
msgid ""
"This saves expensive writes and speeds up operations on volatile data such "
"as web caches and temporary files (like e.g. /tmp and .mozilla) which are "
"regenerated each time. This is achieved by bind mounting each listed "
"directory with a tmpfs on the original path."
msgstr ""

#. type: SH
#: en/live-boot.7:138 en/live-snapshot.1:42
#, no-wrap
msgid "FILES"
msgstr ""

#. type: IP
#: en/live-boot.7:139
#, no-wrap
msgid "B</etc/live/boot.conf>"
msgstr ""

#. type: IP
#: en/live-boot.7:140
#, no-wrap
msgid "B</etc/live/boot.conf.d/>"
msgstr ""

#. type: IP
#: en/live-boot.7:141
#, no-wrap
msgid "B<live/boot.conf>"
msgstr ""

#. type: IP
#: en/live-boot.7:142
#, no-wrap
msgid "B<live/boot.conf.d/>"
msgstr ""

#. type: SH
#: en/live-boot.7:144 en/live-snapshot.1:52
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: en/live-boot.7:146
msgid "I<live-snapshot>(1)"
msgstr ""

#. type: Plain text
#: en/live-boot.7:148 en/live-snapshot.1:56
msgid "I<live-config>(7)"
msgstr ""

#. type: Plain text
#: en/live-boot.7:150 en/live-snapshot.1:58
msgid "I<live-helper>(7)"
msgstr ""

#. type: SH
#: en/live-boot.7:151 en/live-snapshot.1:59
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: en/live-boot.7:153 en/live-snapshot.1:61
msgid ""
"More information about live-boot and the Debian Live project can be found on "
"the homepage at E<lt>I<http://live.debian.net/>E<gt> and in the manual at "
"E<lt>I<http://live.debian.net/manual/>E<gt>."
msgstr ""

#. type: SH
#: en/live-boot.7:154 en/live-snapshot.1:62
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: en/live-boot.7:156 en/live-snapshot.1:64
msgid ""
"Bugs can be reported by submitting a bugreport for the live-boot package in "
"the Debian Bug Tracking System at E<lt>I<http://bugs.debian.org/>E<gt> or by "
"writing a mail to the Debian Live mailing list at E<lt>I<debian-live@lists."
"debian.org>E<gt>."
msgstr ""

#. type: SH
#: en/live-boot.7:157 en/live-snapshot.1:65
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: en/live-boot.7:158 en/live-snapshot.1:66
msgid ""
"live-boot was written by Daniel Baumann E<lt>I<daniel@debian.org>E<gt> for "
"the Debian project."
msgstr ""
